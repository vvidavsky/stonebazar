
/**
 *
 * Written by vlad on 23/01/2020
 */

import CFC from "../firestore/CommonFirestoreCaller";
import Preloader from "@/components/Preloader";

const getStringsFromFirebase = (tableName) => ({
  components: {
    Preloader
  },
  data() {
    return {
      fetching: true,
      strings: {}
    }
  },
  created() {
    CFC.getStringsDataFromFirebase(tableName).then(strs => {
      this.fetching = false;
      this.strings = strs;
    }).catch(() => {
      this.fetching = false;
    })
  }
});

export default getStringsFromFirebase;
