/**
 *
 * Written by vlad on 06/09/2019
 */
import {Firestore, Firebase, Performance} from "../../assets/firestore/firebaseConf";
import {Constants} from "../scripts/Constants";
import AppTools from "../scripts/CommonTools";
import Authentication from "./authentication";

export default class FirestoreCaller {
  /**
   * Данная функция запускается при регистрации пользоватееля чтобы проверить
   * никнэйм введенный пользователем на наличие такого же в базе.
   * @param nick - стринг введеный никнейм из строки регистрации
   * @returns {Promise<boolean>}
   * Влад. 18/01/20
   */
  static checkExistingNick(nick) {
    return Firestore.collection(Constants.FIRE_NICKS)
      .where("nick", "==", nick.toLowerCase()).limit(1).get().then(data => data.empty);
  }

  /**
   * Данная функция получает название таблицы как параметр и запрашивает с базы данных
   * массив со стрингами из этой таблицы
   * @returns {Promise<firebase.database.DataSnapshot>}
   * Влад. 19/01/20
   */
  static getStringsDataFromFirebase(tableName) {
    return Firebase.database().ref(`${Constants.FIRE_S_STRINGS}/${tableName}`).once("value").then(snapshot => {
      if (snapshot.val()) {
        return snapshot.val();
      }
    });
  }

  /**
   * Данная функция запрашивает список объявлений из фаербейса
   * @param path - путь к нужной таблице в базе данных
   * @param filters - филтьры (опционалиный параметр)
   * @param postResults - объект с результатами для конкатинации на случай если есть предидущие результаты
   * @param setVuexAppLoader - функция из Вьюкса которая активирует прилоадер
   * @param onMobile - true если апликация загружена на телефоне
   * @returns {Promise<firebase.firestore.QuerySnapshot>}
   * Влад. 09/09/19
   */
  static getPosts(path, filters, postResults, setVuexAppLoader, onMobile) {
    let requiredAmount = onMobile ? Constants.PHONE_AMOUNT_PER_PAGE : Constants.AMOUNT_PER_PAGE;
    if (!onMobile && window.innerWidth > 1800) {
      requiredAmount = Constants.BIG_AMOUNT_PER_PAGE;
    }
    const rangeFields = ["stone_weight", "watch_diameter", "count_diam", "count_clarity", "count_color", "stone_color"];
    let collection = Firestore.collection(path).where("approved", "==", true);
    setVuexAppLoader(true);
    if (filters) {
      Object.keys(filters).forEach((field) => {
        if (filters[field] && (filters[field].length || filters[field] === true) && !rangeFields.includes(field)) {
          collection = collection.where(field, "==", filters[field]);
        }
        if (field === "watch_diameter" && filters.count_diam) {
          const ranges = AppTools.getRangesForFilters(filters.watch_diameter);
          collection = collection.where(field, 'in', ranges);
        }
        if (field === "stone_color" && filters.count_color) {
          let colorCollection = AppTools.getStoneColorCollection(filters.product);
          const ranges = AppTools.getRangesForFilters(filters.stone_color, colorCollection);
          collection = collection.where(field, 'in', ranges);
        }
      });
    }

    if (postResults && postResults.length) {
      const stAt = postResults[postResults.length - 1].renewed_at_ts;
      collection = collection.orderBy("renewed_at_ts", "desc").startAfter(stAt);
    } else {
      collection = collection.orderBy("renewed_at_ts", "desc");
    }

    const trace = Performance().trace("postsFetch");
    trace.start();
    return collection.limit(requiredAmount).get().then(result => {
      trace.stop();
      setVuexAppLoader(false);
      let hasAllData = false;
      if (result.size) {
        if (result.size < requiredAmount) {
          hasAllData = true;
        }
        if (!postResults) {
          postResults = [];
        }
        postResults = postResults.concat(AppTools.convertFirebaseData(result, onMobile));
        return {
          postResults: postResults,
          hasAllData: hasAllData
        };

      } else return {
        postResults: [],
        hasAllData: hasAllData
      };
    });
  }

  /**
   * Функция запрашивающая из базы отзывы о пользователе (чанками)
   * @param feedsArray массив с чанками для дальнейшей конкатинации
   * @param userId номер пользователя для которого нужно получить отзывы
   * @param hasAllFeedbacks буллеанное значение указывающее на то все ли фидбеки подгружены либо есть еще в базе
   * @return {Promise<firebase.firestore.QuerySnapshot<firebase.firestore.DocumentData>>}
   * Влад. 29/05/20
   */
  static getUserFeedbacks(feedsArray, userId, hasAllFeedbacks) {
    const limitTo = Constants.PHONE_AMOUNT_PER_PAGE;
    let collection = Firestore.collection(`${Constants.FIRE_S_FEEDBACKS}/${userId}/feedbacks`)
      .limit(limitTo)
      .where("approved", "==", true)
      .orderBy("date_created", "desc");

    if (feedsArray && feedsArray.length) {
      collection = collection.startAfter(feedsArray[feedsArray.length - 1].date_created);
    }
    return collection
      .get().then(feedbacks => {
        if (feedbacks.size) {
          if (feedbacks.size < limitTo) {
            hasAllFeedbacks = true;
          }

          if (!feedsArray) {
            feedsArray = [];
          }

          feedsArray = feedsArray.concat(AppTools.convertFirebaseData(feedbacks, true));

          return {
            feedResults: feedsArray,
            hasAllFeedbacks: hasAllFeedbacks
          }
        } else return {
          feedResults: [],
          hasAllFeedbacks: hasAllFeedbacks
        }
      });
  }

  /**
   * Данная функция запрашивает данные пользователя из фаерстора
   * по полученному ID из логин данных
   * @param id - ID пользователя из логин данных
   * @returns {Promise<firebase.firestore.DocumentSnapshot>}
   * Влад. 12/09/19
   */
  static getUserDetails(id) {
    return Firestore
      .collection(Constants.FIRE_S_USERS)
      .doc(id).get().then((userDetails) => {
        if (userDetails.exists) {
          return userDetails.data();
        } else {
          return null
        }
      });
  }

  /**
   * Функция обновляющая в базе пользователя когда он в последний раз был на сайте
   * Влад. 24/04/20
   */
  static updateUserLastVisited() {
    Authentication.auth().onAuthStateChanged(user => {
      if (user) {
        Firestore.doc(`users/${user.uid}`).update('last_visited', new Date().getTime()).then(data => {
        });
      }
    });
  }

  /**
   * Даннная функция запрашивает даннные о просмотрах того или иного
   * объявления и создает объект для построения Чарта в попапе с полными даннными объявления
   * в личном кабинете.
   * @param id номер объявления для запроса
   * @param isPhone булеанное значение обозначающее з моильного дивайса зашел клиент или нет
   * @returns {Promise<firebase.firestore.DocumentSnapshot>}
   * Влад. 27/11/19
   */
  static getPostViews(id, isPhone) {
    const trace = Performance().trace("tracePostViews");
    trace.start();
    return Firestore.doc(`${Constants.FIRE_S_VIEWS}/${id}`).get().then(obj => {
      trace.stop();
      if (obj.exists) {
        const amount = isPhone ? 7 : 14;
        const fbData = obj.data();
        const byDate = fbData.by_date;
        const initialDataArray = [];
        const dataToReturn = {total_views: fbData.total_views, categories: [], series: [{data: []}]};

        Object.keys(byDate).forEach(key => {
          initialDataArray.push(byDate[key]);
        });

        // Сортируем данные по датам в порядке убывания к наиболее давней дате.
        initialDataArray.sort((a, b) => a.time - b.time);

        initialDataArray.forEach(view => {
          dataToReturn.categories.push(view.date);
          dataToReturn.series[0].data.push(view.views);
        });


        // Данные строки отрезают кусок из массивов, так чтобы чарт показывал просмотры только за последний месяц.
        // Не много тупой ход, но с учетом что данных не много и обрабатывать их не сложно, то в начале я создаю масив
        // из всех имеющихся данных а потом отрезаю нужный кусок
        dataToReturn.categories = dataToReturn.categories.slice(dataToReturn.categories.length - amount, dataToReturn.categories.length);
        dataToReturn.series[0].data = dataToReturn.series[0].data.slice(dataToReturn.series[0].data.length - amount, dataToReturn.series[0].data.length);

        return dataToReturn;
      } else {
        return null;
      }
    });
  }
}
