/**
 *
 * Written by vlad on 2019-05-28
 */

import Vue from "vue";
import AppTools from "../scripts/CommonTools";
import {Strings} from "../scripts/Strings";
import {Constants} from "../scripts/Constants";

/**
 * Директива открывающая окно с полными данными объявления
 * передающая в url открытого окна параметры необходимые для
 * запроса полных данных объявления
 * Влад. 28/05/19
 */
Vue.directive("openPost", {
  inserted: function (el, object) {
    el.addEventListener("click", function () {
      let group = object.value.group || "";
      if (window.$nuxt._router.currentRoute.fullPath.includes("stone")) {
        group = "stone";
      }
      if (window.$nuxt._router.currentRoute.fullPath.includes("jewel")) {
        group = "jewelery";
      }
      if (window.$nuxt._router.currentRoute.fullPath.includes("watch")) {
        group = "watch";
      }

      const link = window.$nuxt._router.resolve({path: `/post-view/${group}?id=${object.value.id}`});
      window.open(link.href, "_blank");
    });
  }
});

/**
 * Директива форматирует полученную цену для того чтобы более удобно показать ее пользователю.
 * Влад. 15/06/19
 */
Vue.directive("formatPrice", {
  inserted: function (el, binding) {
    if (binding.value) {
      el.innerHTML = AppTools.formatPrice(binding.value);
    }
  },
  update: function (el, binding) {
    if (binding.value) {
      el.innerHTML = AppTools.formatPrice(binding.value);
    }
  }
});

/**
 * Директва открывающая окно имаила при нажатии на элемент к которому
 * привязана данная деректива. Создана для того чтобы избежать прямой
 * интеерполяции адреса имаила в ДОМе для того чтобы ебаные кроулеры бегающие
 * по сети и охотящиеся на адреса электронной почты не смогли подобрать
 * адрес почты и потом хуячить на него спам, тем самым поднасрав людям.
 * Влад. 23/09/19
 */
Vue.directive("sendEmail", {
  inserted: (el, data) => {
    el.addEventListener("click", function () {
      let product = AppTools.getPostProductString(data.value);
      if (data.value.product === Constants.ITEM_GROUP_WATCH) {
        product = `${product} ${data.value.watch_brand} ${data.value.watch_model} `.trim();
      }
      const a = document.createElement("a");
      const subject = `${Strings.user_qsn_abt_item} ${Strings.app_name}`;
      const body = `${product} за ${data.value.price}$%0D%0A${Strings.url_to_post}: ${window.location.href}`;
      a.href = `mailto:${data.value.email}?subject=${subject}&body=${body}`;
      a.click();
    });
  }
});

/**
 * Данная директива отправляет отчет в гугл аналитикс о навигации по страницам приложения.
 * Влад. 08/01/20
 */
Vue.directive("gtag", {
  inserted: function (el, object) {
    el.addEventListener("click", function () {
      window.$nuxt.$gtag.pageview({
        page_path: window.$nuxt._router.currentRoute.fullPath,
      });
    });
  }
});

/**
 * Данная директива форматирует вес камней так чтоббы значение
 * имело две цифры после точки.
 * Влад. 11/01/19
 */
Vue.directive("weightFormat", {
  inserted: function (el, object) {
    el.innerHTML = AppTools.formatStoneWeight(object.value.post, object.value.field);
  },
  update: function (el, object) {
    el.innerHTML = AppTools.formatStoneWeight(object.value.post, object.value.field);
  }
});
