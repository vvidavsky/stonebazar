/**
 *
 * Written by vlad on 29/05/2020
 */

import LoginDialog from "@/components/dialogs/LoginDialog";
import {Constants} from "../scripts/Constants";

/**
 * Миксин созданный для того чтобы инджектить функциАНАЛ открытия логин дайлога
 * в компоненты.
 * Влад. 29/05/20
 */
export default {
  methods: {
    openLoginDialog() {
      this.$modal.show(LoginDialog, null, {
        width: window.outerWidth < Constants.PHONE_WIDTH ? "95%" : "650",
        height: window.outerWidth < Constants.PHONE_WIDTH ? "auto" : "380",
        clickToClose: false
      });
    }
  }
}
