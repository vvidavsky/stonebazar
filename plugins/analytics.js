/**
 *
 * Written by vlad on 15/11/2019
 */
import Vue from 'vue';
import VueGtag from "vue-gtag";

Vue.use(VueGtag, {
  config: { id: "UA-152536580-1" },
  // includes: [
  //   {id: "UA-162687797-1"} // Analytics for Webmaster Studio
  // ],
  enabled: process.env.NODE_ENV === 'production'
});
