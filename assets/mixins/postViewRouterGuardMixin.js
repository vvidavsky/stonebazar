import {Constants} from "../scripts/Constants";

/**
 *
 * Written by vlad on 09/04/2020
 */

export default {
  beforeRouteEnter (to, from, next) {
    if (to.query.id && to.query.id.length) {
      next();
    } else {
      next(false)
    }
  },
}
