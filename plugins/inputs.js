/**
 *
 * Written by vlad on 2019-07-19
 */

import Vue from 'vue';
import InputComp from "@/components/inputs/input-component";
import DigitsComp from "@/components/inputs/digits-component";
import PickerComp from "@/components/inputs/picker-component";
import PickerCompGroups from "@/components/inputs/picker-component-groups";
import CheckboxComp from "@/components/inputs/checkbox-component";
import RadioComp from "@/components/inputs/radio-button-component";
import LabelComponent from '@/components/inputs/label-component';
import TextareaComponent from '@/components/inputs/textarea-component';
import BrandsAutoComplete from '@/components/inputs/brands-autocomplete';
import SliderComponent from 'vue-slider-component';
import EmptyListPlaceholder from "@/components/EmptyListPlaceholder";


Vue.component("vi-input", InputComp);
Vue.component("vi-digits-input", DigitsComp);
Vue.component("vi-picker", PickerComp);
Vue.component("vi-picker-groups", PickerCompGroups);
Vue.component("vi-checkbox", CheckboxComp);
Vue.component("vi-radio", RadioComp);
Vue.component("vi-label", LabelComponent);
Vue.component("vi-textarea", TextareaComponent);
Vue.component("vi-brands-auto", BrandsAutoComplete);
Vue.component("vi-slider", SliderComponent);
Vue.component("vi-list-placeholder", EmptyListPlaceholder);

