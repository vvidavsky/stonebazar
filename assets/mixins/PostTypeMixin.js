/**
 *
 * Written by vlad on 13/11/2019
 */

import {Constants} from "../scripts/Constants";

export default {
  computed: {
    isStone: function () {
      return this.postGroup === Constants.ITEM_GROUP_STONE;
    },

    isJewl: function () {
      return this.postGroup === Constants.ITEM_GROUP_JEWL;
    },

    isWatch: function () {
      return this.postGroup === Constants.ITEM_GROUP_WATCH;
    }
  }
}
