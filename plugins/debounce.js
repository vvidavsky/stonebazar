/**
 *
 * Written by vlad on 18/01/2020
 */
import Vue from 'vue';
import vueDebounce from 'vue-debounce';

Vue.use(vueDebounce, {
  listenTo: ['input'],
  defaultTime: '1000ms',
  fireOnEmpty: false
});
