/**
 *
 * Written by vlad on 26/02/2020
 */


import {Constants} from "../scripts/Constants";

export default {
  computed: {
    isGem: function () {
      const data = this.postData || this.filtersObject || this.post;
      const gems = ["ruby", "emerald", "sapphire"];
      return gems.includes(data.product);
    }
  }
}
