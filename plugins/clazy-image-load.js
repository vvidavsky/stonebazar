/**
 *
 * Written by vlad on 2019-08-07
 */

import Vue from "vue";
import VueClazyLoad from 'vue-clazy-load'

Vue.use(VueClazyLoad);
