import Vue from 'vue'

export default class Notifications {
  /**
   * Функция выводят оповещение типа "Удачное".
   * @param title - заголовок сообщения
   * @param msg - текст сообщения
   * @param duration - продолжительность отображения сообщения
   * Макс. 13/01/2019
   */
  static showSuccess(title, msg, duration = 5000) {
    Vue.notify({
      group: 'app-default',
      type: 'success',
      title: title,
      text: msg,
      duration: duration
    })
  }

  /**
   * Функция выводят оповещение типа "предупреждение".
   * @param title - заголовок сообщения
   * @param msg - текст сообщения
   * @param duration - продолжительность отображения сообщения
   * Макс. 13/01/2019
   */
  static showWarning(title, msg, duration = 5000) {
    Vue.notify({
      group: 'app-default',
      type: 'warn',
      title: title,
      text: msg,
      duration: duration
    })
  }

  /**
   * Функция выводят оповещение об ошибке.
   * @param title - заголовок сообщения
   * @param msg - текст сообщения
   * @param duration - продолжительность отображения сообщения
   * Макс. 13/01/2019
   */
  static showError(title, msg, duration = 5000) {
    Vue.notify({
      group: 'app-default',
      type: 'error',
      title: title,
      text: msg,
      duration: duration
    })
  }
}
