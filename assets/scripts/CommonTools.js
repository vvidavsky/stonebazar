/**
 * Written by vlad on 09/01/2019
 */

import {Constants} from "./Constants";
import {Strings} from "./Strings";

/**
 * Данный класс хранит инструмменты для общего пользования всеми компонентами
 * приложения. Вычисления, агригации и другая мелкая поеботина.
 * Влад. 09/01/19
 */
export default class AppTools {
  /**
   * Данная фунцтия получает объект с данными которые должен быть сохранены,
   * проводит его валидацию и очищает по мере нужды, после чего возвращает
   * готовый объект с данными для сохранения
   * @param dataToOperate - объект с данными которому нужно провести проверку
   * @returns {*}
   * Влад. 27/01/19
   */
  static validatePersonalDataBeforeSave(dataToOperate) {
    Constants.CONTACT_FIELDS.map((field) => {
      const clonedField = [];
      dataToOperate[field].map(function (tel) {
        if ((tel.number && tel.number.length) || dataToOperate[field].length === 1) {
          // Если пользователь ввел номер телефона начиная с 8 для Вацапа/Вайбера/Телеграма
          // то 8ка заменяется на +7 так как номера в месенджерах всегда указаны с кодом страны
          // и не будут найдены при авто редайректе.
          if (tel.number && tel.number.charAt(0) === "8" && field !== "phone") {
            tel.number = tel.number.replace("8", "+7");
          }
          clonedField.push(tel);
        }
      });
      dataToOperate[field] = clonedField;
    });
    return dataToOperate;
  }

  /**
   * Данная функция проверяет что пользовател заполнил хотябы одно поле с контактными данными
   * @param postData - объект с данными объавления
   * @return {*|number}
   * Влад. 01/04/20
   */
  static hasContactDetails(postData) {
    const atLeastOnePhone = Constants.CONTACT_FIELDS.some(field => postData[field][0].number.length);
    const atLeastOneOther = (postData.email && postData.email.length) || (postData.skype && postData.skype.length);
    return atLeastOnePhone || atLeastOneOther;
  }

  /**
   * Данная функция получает объект с персональными данными зарегистрированного пользователя
   * и проверяет есть ли в контактных полях хотябы один сохраненый номер телефона.
   * @param dataToCheck - объект с персональными данными пользователя.
   * @returns {boolean}
   * Влад. 07/03/19
   */
  static checkExistingContactDetails(dataToCheck) {
    return Constants.CONTACT_FIELDS.some((field) => {
      return dataToCheck[field][0].number.length;
    });
  }

  /**
   * Данная функция получает объект с данными объявления и возвращает объект со сгенерированными
   * линками на данное объявление во всех местах в базе данных для созхранения, удаления, редактирования.
   * @param postData объект с данными объявления
   * @returns {{postFullPath: string, postSlimPath: string, postsPerUserPath: string}}
   * Влад. 03/04/19
   */
  static getAllPostDbReferences(postData) {
    const selectedGroup = this.getPostGroup(postData);
    const pathFull = `posts_russia/full/${selectedGroup}/${postData.id}`;
    const pathSlim = `posts_russia/slim/${selectedGroup}/${postData.id}`;
    const pathToPostsByUser = `${Constants.FIRE_S_POSTS_BY_USER}/${postData.account_id}/posts/${postData.id}`;
    const pathToPostViews = `post_views/${postData.id}`;
    return {
      postFullPath: pathFull,
      postSlimPath: pathSlim,
      postsPerUserPath: pathToPostsByUser,
      postViewsPath: pathToPostViews
    };
  }

  /**
   * Данная функция получает цену как параметр и возвращает ее же отформатированное значение.
   * @param price строка цены
   * @param sign символ валюты
   * @returns {*}
   * Влад. 20/05/19
   */
  static formatPrice(price, sign) {
    return `${this.formatNumber(price)}${sign ? sign : "$"}`;
  }

  /**
   * Данная функция получает число как параметр и возвращает ее же отформатированное значение.
   * С точками, запятыми и другой хуетой
   * @param number строка цены
   * @returns {*}
   * Влад. 26/12/19
   */
  static formatNumber(number) {
    return ('' + number).replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }

  /**
   * Данная функция копирует любую полученную единицу данных и возвращает
   * индивидуальныю единицу данных без поинтеров на оригинальные данные
   * @param data - данные которые нужно склонировать
   * @returns {any}
   * Влад. 07/03/19
   */
  static clone(data) {
    return JSON.parse(JSON.stringify(data));
  }

  /**
   * Функция получающая объект и проверяющая пустой ли он.
   * @param obj - объект который нужно проверить
   * @returns {boolean}
   * Влад. 03/08/19
   */
  static isEmptyObj(obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object;
  }

  /**
   * Данная функция получает МЭП из фаербейса и преобразовывает его в массив
   * затем возвращает.
   * @param data полученный МЭП с данными из фаербейса
   * @param onMobile - is true если аппликация поднята на смартфоне
   * @returns {Array}
   * Влад. 06/08/19
   */
  static convertFirebaseData(data, onMobile) {
    const arr = [];
    const requiredAmount = onMobile ? Constants.PHONE_AMOUNT_PER_PAGE : Constants.AMOUNT_PER_PAGE;
    data.forEach(function (doc) {
      arr.push(doc.data());
    });
    // Здесь мы всегда удаляем последний плученный элемент из массива
    // Он нужен только для того чтобы знать точно что слесующая порция
    // данных существует и можнос тазрешить слшдующий запрос данных
    if (arr.length === requiredAmount) {
      arr.pop();
    }
    return arr;
  }

  /**
   * Функция возвращает актуальннуй класс для списка объявление в зависимости от
   * полученного параметра.
   * @param mode - Стринг указывающий на то в каком режиме находится список
   * @param additionalClass - Класс указывающий на тип листа в зависимости от категории объявления
   * @returns {string}
   * Влад. 10/08/19
   */
  static getListModeClass(mode, additionalClass) {
    return mode && mode === "grid" ? `vi-posts-list-grid ${additionalClass}` : `vi-posts-list-rows ${additionalClass}`;
  }

  /**
   * Фукция возвращающая название товара в таком виде чтоб пользователь мог прочитать
   * а не сидел и тупил над названием
   * @param post - объект содержащий данные объявления
   * @param isForPostForm - true если запрос пришел из нередактируемого поля Товар в форме объявления
   * @returns {*}
   * Влад. 10/08/19
   */
  static getProductString(post, isForPostForm) {
    const group = this.getPostGroup(post);
    let collection;
    switch (group) {
      case Constants.ITEM_GROUP_STONE:
        collection = "post_stones";
        break;
      case Constants.ITEM_GROUP_JEWL:
        collection = "post_jewls";
        break;
    }

    return collection ? Strings.post_form[collection].find(item => item.key === post.product).text : !isForPostForm ? post.watch_brand : Strings.groups.watch;
  }

  /**
   * Функция возвращающая стринг с текстом формы огранки в зависимости
   * от полученного енумератора.
   * @param ctx - component context
   * @returns {string}
   * Влад. 25/08/19
   */
  static getStoneShape(ctx) {
    if (ctx.post) {
      const shape = ctx.astrings.post_form.shape_opts.find(shape => shape.key === ctx.post.stone_shape);
      return shape && shape.text.length ? shape.text : "---";
    }
  }

  /**
   * Данная функция получает объект с енумератором опредееляющим тип метала изделия
   * и возвращает понятный для пользователя текст метала изделия чтобы он бедный не охуевал
   * от увиденного.
   * @param ctx - объект содержащий енумератор метала изделия.
   * @returns {string}
   * Влад. 06/08/19
   */
  static getProductMaterial(ctx) {
    if (ctx.post) {
      const group = this.getPostGroup(ctx.post);
      let material;
      switch (group) {
        case Constants.ITEM_GROUP_JEWL:
          material = ctx.astrings.post_form.material_opts.find(material => material.key === ctx.post.material);
          break;
        case Constants.ITEM_GROUP_WATCH:
          material = ctx.astrings.post_form.watch_metal_opts.find(material => material.key === ctx.post.watch_material);
          break;
      }
      return material && material.key ? material.text : "---";
    }
  }

  /**
   * Данная хуета геенерирует линки для автоматического открытия чата
   * в подходящем приложении.
   * @param platform - String Whatsapp/Telegram/Viber
   * @param phone - объект с номером телефона
   * @returns {string}
   * Влад. 22/09/19
   */
  static getPhonesLinks(platform, phone) {
    if (this.isMobile()) {
      switch (platform) {
        case "phone":
          return `tel:${phone.number}`;
        case "whatsapp":
          return `https://wa.me/${phone.number.replace("+", "")}`;
        case "telegram":
          return `https://telegram.me/${phone.number}`;
        case "viber":
          return `viber://chat/?number=${phone.number.replace("+", "%2B")}`;
      }
    } else {
      return "";
    }
  }

  /**
   * Данная функция собирает все имеющиеся номера телефонов в одну группу форматируя данные и добавляя нужный
   * класс иконки в объект для удобной и эфективной итерации в момент рендеринга view.
   * @param data - является объектом содержащим разные типы номеров телефонов Вацап, Вайбер и хуё моё.
   * @returns {Array}
   * Влад. 05/06/19
   */
  static joinPhoneNumbersArrays(data) {
    const fieldsToJoin = Constants.CONTACT_FIELDS;
    const phonesListToReturn = [];
    fieldsToJoin.map(field => {
      if (data[field] && data[field].length) {
        data[field].map(phone => {
          if (phone.number) {
            const newPhoneObject = {
              className: `vi-${field}-i`,
              number: `${phone.number}`,
              appName: field === "phone" ? "Номер телефона" : field,
              link: this.getPhonesLinks(field, phone)
            };
            phonesListToReturn.push(newPhoneObject);
          }
        });
      }
    });
    return phonesListToReturn;
  }

  /**
   * Данная функция получает объект содержащий поля и булеанное значение полей
   * и возвращает только те поля чье значение является true.
   * @param details - object with fields to check
   * @returns {string[]}
   * Влад. 06/06/19
   */
  static getOnlyTrueValues(details) {
    if (details) {
      return Object.keys(details).filter(field => details[field] === true);
    }
  }

  /**
   * Функция узнающая к какой категории принадлежит объявление.
   * @param post - объект объявления которое нужно проверить
   * @returns {*}
   * Влад. 17/09/19
   */
  static getPostGroup(post) {
    if (post.product && post.product.length) {
      return Strings.post_form.group_ident.find(group => {
        return group.ident.includes(post.product) || group.ident === Constants.ITEM_GROUP_WATCH;
      }).id;
    } else {
      return null;
    }
  }

  /**
   * Данная фуннкция получает объект с данными объявления и возвращает стринг с названием
   * продукта (Кольцо/Бриллиант/Часы и тд).
   * @param post объект с данными объявления
   * @returns {null}
   * Влад. 05/11/19
   */
  static getPostProductString(post) {
    const groupStrings = {stone: "post_stones", jewelery: "post_jewls", watch: "post_watches"};
    const group = this.getPostGroup(post);
    if (group) {
      const found = Strings.post_form[groupStrings[group]].find(item => item.key === post.product);
      return found ? found.text : null;
    } else {
      return null;
    }
  }

  /**
   * Функция получающая дату таймстемпом и возвращающая разницу в днях по сравнению с сегодяшней датой
   * @param moment - инстанс moment.js
   * @param targetDate - date timestamp для сравнения
   * @param toHours - is TRUE если нужно вернуть разницу в часах
   * @returns {number | * | [] | string}
   * Влад. 09/10/19
   */
  static getTimeDiff(moment, targetDate, toHours) {
    const today = moment(new Date());
    const timeTo = moment(targetDate);
    if (toHours) {
      const diff = moment.duration(timeTo.diff(today)).asHours();
      return Math.floor(diff);
    } else {
      return timeTo.diff(today, "days");
    }
  }

  /**
   * Функция получающЯ таймстемп и возвращающая время прошедшее с момента о обращения
   * к данной функции до момента указанного в таймстемпе. Пока что возвращает
   * вреемя в днях, но в будущем возможно будут добавлены опции возвррата в минутах, часах и тд. и хуё моё
   * @param moment - инстанс moment.js
   * @param targetDate - date timestamp для сравнения
   * @return {number}
   * Влад. 23/02/20
   */
  static getTimePassed(moment, targetDate) {
    const now = moment(new Date());
    const timeTo = moment(targetDate);
    const diff = moment.duration(now.diff(timeTo)).asDays();
    return Math.floor(diff);
  }

  static isMobile() {
    return !!(navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i));
  }

  static isIos() {
    return !!navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/iPad/i)
  }

  /**
   * Данная функция форматирует вес камня и возвращает значение с
   * сотым значением после точки (Х.00);
   * @param post - объект с данными в котором находится значение которое нужно отформатировать
   * @param field - поле из объекта, чье значение нужно отформатировать
   * @returns {string|*}
   * Влад. 11/01/19
   */
  static formatStoneWeight(post, field) {
    const weight = post[field];
    return weight ? `${weight.toFixed(2)}ct.` : "---";
  }

  /**
   * Данная функция получает массив с минимальным и макцимальным числом
   * и генерирует между ними все не хватающие числа.
   * Используется при генерации ренджей для фильтров
   * @param rangeArray массив содержащий минимальное и максимальное число [1, 10]
   * @param collection (optional) название коллекции со стрингами из которой нужноо вытащить конкретныее параметры
   * @returns {unknown[]}
   * Влад. 29/01/20
   */
  static getRangesForFilters(rangeArray, collection) {
    const start = rangeArray[0];
    const end = rangeArray[1];
    if (collection) {
      return Strings.post_form[collection].filter((element, i) => i >= start && i <= end).map(el => el.text);
    } else {
      return Array(end - start + 1).fill().map((_, idx) => start + idx)
    }
  }

  /**
   * Функция получает тип камня и возвращает подходящее название коллекции с цветом
   * данного камня.
   * @param stone (String) - инумератор названия камня
   * @returns {string}
   * Влад. 10/02/20
   */
  static getStoneColorCollection(stone) {
    switch (stone) {
      case "diamond":
        return "diamond_color_opts";
      case "ruby":
        return "ruby_color_opts";
      case "emerald":
        return "emerald_color_opts";
      case "sapphire":
        return "sapphire_color_opts";
    }
  }

  /**
   * Функция узнающая режим Dark Mode в IOS устройствах и возвращающая булеанное значение.
   * @return {boolean}
   * Влад. 20/03/20
   */
  static isDarkMode() {
    return window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
  }

  /**
   * Данная ебота возвращаеет данные о категории опуликованного драгоценного камня согласно полученому
   * весу камня.
   * Запускается при создании нового объявления.
   * @param weight (Double) - вес камня
   * @returns
   * Влад. 10/02/20
   */
  static getStoneWeightCategory(weight) {
    if (weight) {
      const basicWeight = Math.floor(weight);
      if (basicWeight >= 0 && basicWeight < 1) {
        return Constants.WEIGHT_GR_1
      }
      if (basicWeight >= 1 && basicWeight < 2) {
        return Constants.WEIGHT_GR_2
      }
      if (basicWeight >= 2 && basicWeight < 3) {
        return Constants.WEIGHT_GR_3
      }
      if (basicWeight >= 3 && basicWeight < 4) {
        return Constants.WEIGHT_GR_4
      }
      if (basicWeight >= 4 && basicWeight < 5) {
        return Constants.WEIGHT_GR_5
      }
      if (basicWeight >= 5 && basicWeight < 10) {
        return Constants.WEIGHT_GR_6
      }
      if (basicWeight >= 10 && basicWeight < 50) {
        return Constants.WEIGHT_GR_7
      }
      if (basicWeight >= 50 && basicWeight < 100) {
        return Constants.WEIGHT_GR_8
      }
      return null;
    } else {
      return null;
    }
  };
}
