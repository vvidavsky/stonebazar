/**
 *
 * Written by vlad on 2019-07-18
 */

import Vue from 'vue';
import Notifications from "vue-notification";

Vue.use(Notifications);
