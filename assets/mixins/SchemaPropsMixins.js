/**
 *
 * Written by vlad on 25/04/2020
 */
import {Constants} from "../scripts/Constants";

export default {
  computed: {
    getNameItemProp: (ctx) => {
      return ctx.post.product === Constants.ITEM_GROUP_WATCH ? "brand" : "name";
    },
  }
}
