/**
 *
 * Written by vlad on 23/03/2020
 */

export default class Licensing {

  static licenseTypes() {
    return {
      basic: ["basic"]
    }
  }

  /**
   * Функция проверяющая лицензию пользователя на выполнения того или иного функционала
   * @param user - объект с данными пользователя
   * @param requiredType - треуюемый тип лицензии
   * @return {boolean}
   * Влад. 23/03/20
   */
  static checkLicense(user, requiredType) {
    if (user.is_premium) {
      return true;
    } else {
      switch (requiredType) {
        case "basic":
          return this.licenseTypes().basic.includes(user.license_type);
      }
    }
  }
}
