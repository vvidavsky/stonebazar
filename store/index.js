/**
 *
 * Written by vlad on 2019-07-12
 */
import {Strings} from "assets/scripts/Strings";
import {Constants} from "assets/scripts/Constants";

export const state = () => ({
  astrings: Strings,
  istrings: {}, // Imported strings from firebase
  icities: [],  // Imported cities from firebase
  iwatch_brands: [], // Imported watch brands from firebase
  refresh_new_post: false, // Override key for IOS devices
  user_details: {},
  cached_personal_details: null,
  show_app_loader: false,
  form_request_sent: false,
  show_posts_loader: false,
  posts_filters: null,
  currency: {},
  is_phone: window.innerWidth < Constants.PHONE_WIDTH,
  list_mode: localStorage.getItem("listMode") ? localStorage.getItem("listMode") : "grid"
});

export const mutations = {
  setVuexUserDetails(state, details) {
    state.user_details = details;
  },
  setVuexContactDetails(state, details) {
    state.user_details.contact_details = details
  },
  setVuexCacheForPersonalDetails(state, value) {
    state.cached_personal_details = value;
  },
  setVuexImportedStrings(state, strings) {
    state.istrings = strings;
  },
  setVuexImportedCities(state, cities) {
    state.icities = cities;
  },
  setVuexWatchBrands(state, brands) {
    state.iwatch_brands = brands;
  },
  setVuexAppLoader(state, value) {
    state.show_app_loader = value;
  },
  setVuexPostsLoader(state, value) {
    state.show_posts_loader = value;
  },
  setVuexPostResultsFilter(state, filters) {
    state.posts_filters = filters;
  },
  setVuexListMode(state, mode) {
    localStorage.setItem("listMode", mode);
    state.list_mode = mode;
  },
  setVuexIsPhone(state, value) {
    state.is_phone = value;
  },
  setVuexFormRequestState(state, value) {
    state.form_request_sent = value;
  },
  setVuexCurrencyData(state, value) {
    state.currency = value;
  },
  setVuexRefreshPage(state, value) {
    state.refresh_new_post = value
  }
};
