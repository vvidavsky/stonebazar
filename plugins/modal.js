/**
 *
 * Written by vlad on 2019-07-18
 */

import Vue from 'vue';
import VueModals from 'vue-js-modal';

Vue.use(VueModals, {dynamic: true});
