import {Constants} from "../scripts/Constants";

/**
 *
 * Written by vlad on 21/01/2020
 */

import AppTools from "../scripts/CommonTools";

/**
 * Этот миксин был сделан для того чтобы в момент открытия галлерееи с картинками
 * на айфонах главны контейнер галлереи временно получал overflow: visible чтобы
 * галлерея открывалась во весь экран. А на новой версии ИОС на айфонах она с какого то
 * ебучего хуя не открываеется.
 * Влад. 21/01/20
 */
export default {
  data() {
    return {
      iphoneGallery: false
    }
  },
  created() {
    const self = this;
    if (AppTools.isMobile()) {
      this.$bus.off(Constants.BUS_GALLERY_OPEN);
      this.$bus.on(Constants.BUS_GALLERY_OPEN, function (state) {
        self.iphoneGallery = state;
      });
    }
  }
}
