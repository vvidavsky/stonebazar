/**
 *
 * Written by vlad on 22/09/2019
 */

import AppTools from "./CommonTools";
import {Constants} from "./Constants";

export default class AppConfigurations {

  /**
   * Данная функция отменяет скейлинг вьюпорта в мобильных дивайсах. Ибо нехуй увеличивать
   * и уменьшать его. данная аппликация сексуально адаптированна под все нужды.
   * Влад. 22/09/19
   */
  static disableMobileScaling() {
    if (AppTools.isMobile()) {
      document.addEventListener('gesturestart', function (e) {
        e.preventDefault();
        document.body.style.zoom = 1;
      });

      document.addEventListener('gesturechange', function (e) {
        e.preventDefault();
        document.body.style.zoom = 1;
      });

      document.addEventListener('gestureend', function (e) {
        e.preventDefault();
        document.body.style.zoom = 1;
      });
    }
  }

  /**
   * Даннная функция запускается при инициализации приложения и активирует вочер
   * который следит за изменением размеров вьюпорта, тем самымопределяя находится ли пользователь
   * на экране смарфона или нет.
   * @param setVuexIsPhone функция которая изменяет значеение во Вьюксе указывающее на то находится ли пользователь
   * на смартфоне или нет.
   * Влад. 07/10/19
   */
  static initViewportResizeWatcher(setVuexIsPhone) {
    window.addEventListener('resize',  () => {
      let value = window.innerWidth < Constants.PHONE_WIDTH;
      setVuexIsPhone(value);
    });
  }
}
