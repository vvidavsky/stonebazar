/**
 *
 * Written by vlad on 2019-07-18
 */

import {uuid} from 'vue-uuid';

export const Constants = {

  /*********** FIREBASE TABLES REFERENCES *************/
  FIRE_S_USERS: "users",
  FIRE_S_USERS_PUB: "users_public",
  FIRE_S_POSTS_BY_USER: "posts_by_user",
  FIRE_S_STRINGS: "imported_strings",
  FIRE_S_POSTS_RUS: "posts_russia",
  FIRE_S_VIEWS: "post_views",
  FIRE_S_FEEDBACKS: "users_feedbacks",
  FIRE_NICKS: "existing_nicknames",
  /********* & FIREBASE TABLES REFERENCES & ***********/

  // IPS_API: "http://api.ipapi.com/api/check?access_key=36932dd46b914e28387ce28d004e4382", // Fetch user city url
  FUNCTIONS_API: "https://us-central1-stonesbazar.cloudfunctions.net/",
  CURRENCIES_API: "https://api.currencylayer.com/live?access_key=9a89b00387740868a3fad5bb48165b5b&currencies=USD,RUB",

  /********* FIREBASE ERROR CODES ***********/
  FIRE_ERR_NO_USER: "auth/user-not-found",
  FIRE_ERR_WRONG_PASS: "auth/wrong-password",
  FIRE_ERR_EMAIL_IN_USE: "auth/email-already-in-use",
  /******** & FIREBASE ERROR CODES & ********/

  /************ APP CONFIGURATION *********************/
  MAX_ALLOWED_PHONES: 2,
  POST_LIFE_SPAN: 90, // Days
  POST_LIFE_EXTEND: 30, // Days
  POST_LIFE_RED_LINE: 10, // Days
  PHONE_WIDTH: 700, //PX
  MAX_ALLOWED_IMAGES: 7, //amount of allowed images for per post
  MAX_FILE_IMAGE_SIZE: 8, //maximal allowed image size to upload
  AMOUNT_PER_PAGE: 21,
  PHONE_AMOUNT_PER_PAGE: 11,
  BIG_AMOUNT_PER_PAGE: 51,
  /********** & APP CONFIGURATION & *******************/

  /************ BUS EVENT NAMES *********************/
  BUS_SAVE_POST: "save_post",
  BUS_RESET_DZONE: "reset_drop_zone",
  BUS_UPD_POST_EXP: "upd_post_live",
  BUS_GALLERY_OPEN: "gallery_event",
  /********** & BUS EVENT NAMES & *******************/

  /************ MOMENT FORMATS ***********************/
  MT_FULL_DATE: "DD.MM.YYYY",
  /********** & MOMENT FORMATS & *********************/

  SK: "dina",

  CONTACT_FIELDS: ["phone", "whatsapp", "telegram", "viber"],
  ITEM_GROUP_STONE: "stone",
  ITEM_GROUP_JEWL: "jewelery",
  ITEM_GROUP_WATCH: "watch",

  WEIGHT_GR_1: "0-0.99",
  WEIGHT_GR_2: "1-1.99",
  WEIGHT_GR_3: "2-2.99",
  WEIGHT_GR_4: "3-3.99",
  WEIGHT_GR_5: "4-4.99",
  WEIGHT_GR_6: "5-9.99",
  WEIGHT_GR_7: "10-49.99",
  WEIGHT_GR_8: "50-99.99",

  STONES_FILTERS: {
    product: null,
    stone_shape: null,
    stone_weight: null,
    stone_color: [1, 3],
    stone_clarity: null,
    stone_origin: null,
    certificate: null,
    city: null,
    weight_category: null,
    count_clarity: false,
    count_color: false,
  },

  JEWLS_FILTERS: {
    product: null,
    stone: null,
    certificate: null,
    material: null,
    city: null,
    size: null,
    jewel_brand: null
  },

  WATCHES_FILTERS: {
    watch_brand: null,
    watch_material: null,
    box: false,
    city: null,
    watch_strap: null,
    gender: null,
    condition: null,
    watch_mechanism: null,
    watch_document: null,
    watch_diameter: [30, 39],
    count_diam: false,
    watch_shape: null
  },

  POST_SCHEME: {
    account_id: null,
    approved: false,
    rejected: false,
    created_at_ts: null,
    renewed_at_ts: null,
    expires_at_ts: null,
    product: null,
    stone_shape: null,
    material: null,
    metal_content: null,
    stone: null,
    central_weight: null,
    stone_weight: null,
    stone_color: null,
    stone_clarity: null,
    stone_origin: null,
    weight_category:null,
    total_stones_weight: null,
    jewel_brand: null,
    certificate: null,
    watch_brand: null,
    watch_material: null,
    watch_model: null,
    watch_strap: null,
    watch_mechanism: null,
    watch_document: null,
    watch_diameter: null,
    watch_shape: null,
    condition: null,
    size: null,
    box: null,
    description: null,
    email: null,
    skype: null,
    city: null,
    contact_name: null,
    price: null,
    gender: null,
    images: [],
    thumbnail: null,
    phone: [
      {
        number: "",
        uuid: uuid.v1()
      }
    ],
    whatsapp: [
      {
        number: "",
        uuid: uuid.v1()
      }
    ],
    telegram: [
      {
        number: "",
        uuid: uuid.v1()
      }
    ],
    viber: [
      {
        number: "",
        uuid: uuid.v1()
      }
    ]
  },

  USER_CONTACT_DET: {
    phone: {
      number: ""
    },
    whatsapp: {
      number: ""
    },
    viber: {
      number: ""
    },
    telegram: {
      number: ""
    },
    skype: null,
    email: null,
    avatar: null,
    contact_name: null,
    city: null
  }
};
