export default {
  ssr: false,
  /*
  ** Headers of the page
  */
  head: {
    title: 'Saknay.ru - покупка и продажа ювелирных изделий, драгоценных камней и швейцарских часов',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=2.0'},
      {name: 'yandex-verification', content: '3d41d0271fdac885'},
      {name: 'wmail-verification', content: 'ec880c45976602e05f0ee5f62b59d896'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''},
      {
        hid: 'keywords',
        name: 'keywords',
        content: "сакнай.ру, продажа ювелирных изделий, продать бриллиант, продать сапфир, " +
          "продать рубин, продать кольцо, купить часы, продать часы, швейцарские часы продажа, купить швейцарские часы," +
          "золото, купить кольцо с бриллиантом, продать кольцо с бриллиантом, кольцо с сапфиром, кольцо с рубином, " +
          "серьги с рубином, серьги с сапфиром, серьги с изумрудом, купить запонки, продать запонки, продать колье, подать корону," +
          "купить корону, купить колье, купить драгоценные камни, продать бриллиант Москва, продать кольцо с бриллиантом Москва," +
          "продать серьги из золота Москва, продать рубин Москва, купить Москва, купить кольцо с бриллиантом Москва"
      },
      {hid: 'og:type', name: 'og:type', content: 'website'},
      {hid: 'og:title', name: 'og:title', content: 'Saknay.ru'},
      {hid: 'og:image', name: 'og:image', content: '/android-chrome-512x512.png'},
      {hid: 'og:locale', name: 'og:locale', content: 'ru_RU'},
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'покупка и продажа ювелирных изделий, драгоценных камней и швейцарских часов'
      },
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  generate: {
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [
    '@/assets/styles/main.css',
    '@/assets/styles/icons.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/modal',
    '~/plugins/notifications',
    '~/plugins/vee-validate',
    '~/plugins/inputs',
    '~/plugins/bus',
    '~/plugins/clazy-image-load',
    '~/plugins/tooltip',
    '~/plugins/analytics',
    '~/plugins/debounce',
    '~/plugins/crypto',
    '~/plugins/star-rating',
  ],
  /*
  ** Nuxt.js modules
  */
  buildModules: [
    ['@nuxtjs/yandex-metrika', {
      id: '61601977',
      webvisor: true,
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true
    }],
    'nuxt-facebook-pixel-module',
    '@nuxtjs/moment',
    '@nuxtjs/robots',
    //'@nuxtjs/sitemap'
  ],
  facebook: {
    track: 'PageView',
    pixelId: '624592788484306',
    disabled: false
  },
  moment: {
    defaultLocale: 'ru',
    locales: ['ru']
  },
  robots: [
    {
      UserAgent: "Yandex",
      CrawlDelay: 5,
      Disallow: "/cabinet/",
      Host: "https://saknay.ru/",
      Sitemap: "https://saknay.ru/sitemap.xml"
    },
    {
      UserAgent: "Googlebot",
      CrawlDelay: 5,
      Disallow: "/cabinet/",
      Sitemap: "https://saknay.ru/sitemap.xml"
    },
    {
      UserAgent: "*",
      CrawlDelay: 5,
      Disallow: "/cabinet/",
      Sitemap: "https://saknay.ru/sitemap.xml"
    }
  ],
  sitemap: {
    hostname: "https://saknay.ru",
    sitemaps: [
      {
        path: "sitemap.xml",
        exclude: [
          '/prices',
          '/legal-notes',
          '/posts',
          '/post-view/*',
          '/cabinet',
          '/cabinet/*'
        ]
      },
      {
        path: "saknay_sitemap_full_xpt.xml",
        routes: async () => {
          const {Firestore} = require("./assets/firestore/firebaseConf");
          const allPaths = ["jewelery", "stone", "watch"];
          const allPromises = allPaths.map(path => {
            return new Promise(resolve => {
              Firestore.collection(`/posts_russia/slim/${path}`)
                .where("approved", "==", true)
                .limit(500)
                .get().then(data => resolve(data));
            });
          });

          return Promise.all(allPromises).then(results => {
            const genRoutes = [];
            const landings = ["stones", "jewels", "watches"];
            landings.forEach(land => {
              genRoutes.push(              {
                url: `/s-${land}-desc.html`,
                changefreq: "weekly",
                priority: 1,
                lastmod: new Date()
              })
            });
            results.map((collection, index) => {
              collection.forEach(post => {
                genRoutes.push({
                  url: `/post-view/${allPaths[index]}/?id=${post.data().id}`,
                  changefreq: "daily",
                  priority: 1,
                  lastmod: new Date()
                });
              });
            });
            return genRoutes;
          });
        },
        exclude: [
          '/prices',
          '/legal-notes',
          '/posts',
          '/post-view/*',
          '/cabinet',
          '/cabinet/*'
        ]
      }
    ]
  },
  /*
  ** Build configuration
  */
  build:
    {
      extend(config, ctx) {

      }
    }
}
