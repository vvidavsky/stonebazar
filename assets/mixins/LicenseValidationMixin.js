/**
 *
 * Written by vlad on 23/03/2020
 */

import Licensing from "../scripts/Licensing";

export default {
  computed: {
    basicLicenseValid: (ctx) => {
      return Licensing.checkLicense(ctx.user_details, "basic");
    }
  }
}
