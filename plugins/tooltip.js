/**
 *
 * Written by vlad on 13/09/2019
 */

import VTooltip from "v-tooltip";
import Vue from 'vue';

Vue.use(VTooltip);
