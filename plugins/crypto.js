/**
 *
 * Written by vlad on 26/01/2020
 */
import Vue from 'vue';
import VueCryptojs from 'vue-cryptojs';

Vue.use(VueCryptojs);
