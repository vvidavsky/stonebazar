/**
 *
 * Written by vlad on 26/12/2019
 */

import Axios from "axios";
import {Constants} from "./Constants";

export default class GateKeeper {
  static getCurrencies() {
    return Axios.get(Constants.CURRENCIES_API).then((resp) => resp.data.success ? resp.data : null);
  }
}
