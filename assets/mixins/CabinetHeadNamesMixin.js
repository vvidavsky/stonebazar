/**
 *
 * Written by vlad on 14/05/2020
 */
import {Strings} from "../scripts/Strings";

/**
 * Данный миксин получает поинтер обозначающий страницу в кабинете и возврашающая заголовок
 * страницы для отображения в хедере
 * @param pointer (стринг)
 * @param dictionary - стринг указывающий в какой коллекции стрингов искать слово
 * @return {{computed: {getName: computed.getName}}}
 * Влад. 14/05/20
 */
const getHeaderName = (pointer, dictionary = "cabinet_links") => ({
  computed: {
    getName: () => {
      if (pointer && pointer.length) {
        return Strings[dictionary].find(link => link.icon === pointer).name;
      } else {
        return "N/A";
      }
    }
  }
});

export default getHeaderName;
