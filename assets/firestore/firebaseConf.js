/**
 *
 * Written by vlad on 09/01/19
 */

import Firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/storage';
import 'firebase/firestore';
import 'firebase/functions';
import "firebase/performance";

let config = {
  apiKey: " AIzaSyC6pA984HIfJipXY2cmz2v55uYlLAnqG6g",
  authDomain: "stonesbazar.firebaseapp.com",
  databaseURL: "https://stonesbazar.firebaseio.com",
  projectId: "stonesbazar",
  storageBucket: "stonesbazar.appspot.com",
  messagingSenderId: "253796812498",
  appId: "1:253796812498:web:b061d5ea2ddb52bb"
};

// if (process.env.NODE_ENV === 'production') {
//   config = {
//     apiKey: " AIzaSyC6pA984HIfJipXY2cmz2v55uYlLAnqG6g",
//     authDomain: "stonesbazar.firebaseapp.com",
//     databaseURL: "https://stonesbazar.firebaseio.com",
//     projectId: "stonesbazar",
//     storageBucket: "stonesbazar.appspot.com",
//     messagingSenderId: "253796812498",
//     appId: "1:253796812498:web:b061d5ea2ddb52bb"
//   };
// } else {
//   config = {
//     apiKey: "AIzaSyCrtS8XLgaClVVEcGKn11_t7PX8uM5YZD0",
//     authDomain: "stonesbazar-staging.firebaseapp.com",
//     databaseURL: "https://stonesbazar-staging.firebaseio.com",
//     projectId: "stonesbazar-staging",
//     storageBucket: "stonesbazar-staging.appspot.com",
//     messagingSenderId: "386921961006",
//     appId: "1:386921961006:web:675ba98948e1117a7b31bf"
//   };
// }

Firebase.initializeApp(config);

const Firestore = Firebase.firestore();
const Performance = Firebase.performance;

export  {Firebase, Firestore, Performance};
