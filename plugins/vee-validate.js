/**
 *
 * Written by vlad on 2019-07-18
 */

import Vue from 'vue';
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);
