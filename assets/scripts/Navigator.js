/**
 *
 * Written by vlad on 29/09/2019
 */
export default class Navigator {
  constructor(router) {
    this.router = router;
  }

  goToMainView() {
    this.router.push("/");
  }

  goToCabinet() {
    this.router.push("/cabinet/profile");
  }

  goToCreatePost() {
    this.router.push("/cabinet/new-post")
  }
}
