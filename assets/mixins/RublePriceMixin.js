/**
 *
 * Written by vlad on 26/12/2019
 */

import AppTools from "../scripts/CommonTools";

/**
 * Данное вычисляемое переводит введеную сумму в долларах в рубли и отображает
 * в ячейке ЦЕНА В РУБЛЯХ.
 * Влад. 26/12/19
 */
const getRubbleValue = (data, currency) => ({
  computed: {
    getRubPrice: function () {
      if (this.currency.rub) {
        if (this[data].price) {
          if (currency) {
            return AppTools.formatPrice(parseInt(this[data].price * parseFloat(this.currency.rub)), "руб.");
          } else {
            return AppTools.formatNumber(parseInt(this[data].price * parseFloat(this.currency.rub)));
          }
        } else {
          return 0;
        }
      } else {
        return "";
      }
    },
  }
});

export default getRubbleValue;
