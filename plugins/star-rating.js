/**
 *
 * Written by vlad on 26/05/2020
 */

import StarRating from "vue-star-rating";
import Vue from "vue";

Vue.component('star-rating', StarRating);
